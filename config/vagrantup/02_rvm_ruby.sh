#!/usr/bin/env bash

if [[ ! $(rvm -v) ]]; then
    echo "Installing RVM..."
    curl -sSL https://rvm.io/mpapis.asc | gpg --import
    curl -L https://get.rvm.io | bash -s stable
fi
source /etc/profile.d/rvm.sh

rvm install 2.2.2

rvm 2.2.2 do gem install bundle

echo "Preparing image loader API server..."
cd /vagrant
bundle install

cat .env || echo -e "PATH=$PATH\nGEM_HOME=$GEM_HOME\nGEM_PATH=$GEM_PATH" >> .env

echo "Exporting to upstart..."
rvm 2.2.2 do foreman export upstart /etc/init -a image_loader -u vagrant

echo "Starting image loader API server"
service image_loader restart
