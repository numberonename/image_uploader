#!/usr/bin/env bash


if [[ ! $(type npm) ]]; then
    echo "Installing nodejs..."
    apt-get install -y nodejs npm
fi

ln -s /usr/bin/nodejs /usr/bin/node

if [[ ! $(type git) ]]; then
    echo "Installing git..."
    apt-get install -y git
fi

if [[ ! $(type redis-server) ]]; then
    echo "Installing redis..."
    add-apt-repository ppa:chris-lea/redis-server
    apt-get -y update
    apt-get -y install redis-server
fi


if [[ ! $(type mongo) ]]; then
    echo "Installing MongoDB"
    mkdir -p /data/db
    chown 777 -R /data
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
    echo "deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen" | sudo tee /etc/apt/sources.list.d/mongodb.list
    apt-get update -y
    apt-get install -y mongodb-org
fi

apt-get -y install libgdbm-dev libncurses5-dev automake libtool bison libffi-dev imagemagick
