require 'mongoid'
require 'autoinc'
require 'sidekiq'
require 'aasm'
require 'mongoid/enum'
require 'mongoid_paperclip'
require 'active_support/all'
require 'grape'
require 'grape-swagger'

ENV['IMAGE_LOADER_ENV'] ||= 'development'
ENV['IMAGE_LOADER_ROOT'] ||= File.expand_path('../..', File.dirname(__FILE__))

Mongoid.load!("#{Dir.pwd}/config/mongoid.yml", ENV['IMAGE_LOADER_ENV'])

project_root = ENV['IMAGE_LOADER_ROOT']

relative_load_paths = Dir["#{project_root}/app/api/**/"]

ActiveSupport::Dependencies.autoload_paths += relative_load_paths

Dir[
  "#{project_root}/config/initializers/**/*.rb",
  "#{project_root}/lib/**/*.rb",
  "#{project_root}/app/models/**/*.rb",
  "#{project_root}/app/workers/**/*.rb"
].each {|file| require file }
