# Sidekiq.configure_client do |config|
#   config.redis = {
#     namespace: 'image_loader',
#     url: 'redis://127.0.0.1:6379/1'
#   }
# end
#
# Sidekiq.configure_server do |config|
#   config.redis = {
#     namespace: 'image_loader',
#     url: 'redis://127.0.0.1:6379/1'
#   }
# end

ENV['RACK_ENV'] = 'development' unless (defined?(ENV['RACK_ENV']) || defined?(ENV['RAILS_ENV']))
ENV['IMAGE_LOADER_ENV'] = ENV['RACK_ENV'] = ENV['RAILS_ENV'] = [ENV['RACK_ENV'], ENV['RAILS_ENV']].compact.first

require_relative 'environments/all'
