Paperclip.interpolates :ext_from_type do |attachment, _|
  attachment.instance.attachment_content_type.gsub(%r{image\/}, '')
end

Paperclip.interpolates :nid do |attachment, _|
  attachment.instance.numeric_id
end

Paperclip.interpolates :job_id do |attachment, _|
  attachment.instance.job.numeric_id
end

Paperclip.interpolates :il_root do |*|
  ENV['IMAGE_LOADER_ROOT']
end
