# require 'rack/cors'
require 'goliath'
require 'em-http-request'

module ImageLoader
  class Server < Goliath::API
    use Rack::Static, root: 'public', urls: ['/swagger', '/system']
    def response(env)
      case env['PATH_INFO']
        when '/'
          [200, {'Content-Type' => 'text/plain'}, 'Please refer to API at /api or to documentation at /docs']
        when '/docs'
          Rack::Response.new.tap do |r|
            r.redirect('swagger/index.html')
            r.finish
          end
        else
          ImageLoader::Api::MainApi.call(env)
      end
    end

    def on_close(_env)
      ActiveSupport::Dependencies::clear if Goliath.env? :development
    end
  end
end
