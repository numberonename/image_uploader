require 'nokogiri'
require 'addressable/uri'
require 'open_uri_redirections'

module ImageLoader
  class JobProcessor
    def initialize(id)
      @job = Job.find(id)
    end

    def process_images
      @job.ready_for_upload!
      job_url = correct_url(@job.url)
      Nokogiri::HTML(open(job_url, allow_redirections: :safe)).xpath('//img/@src').each do |src|
        image_url = Addressable::URI.join(job_url, src).to_s
        @job.images.create(url: image_url) unless @job.images.where(url: image_url).first
      end
      @job.success!
    rescue SocketError
      fail_job("Could not access URL: #{job_url}")
    end

    private

    def correct_url(url)
      u = Addressable::URI.parse url
      u = Addressable::URI.parse("http://#{url}") unless (u.scheme && url['http://'])
      u.to_s
    end

    def fail_job(message)
      @job.last_error = message
      @job.failure
      @job.save
    end
  end
end