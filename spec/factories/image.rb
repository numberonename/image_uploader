FactoryGirl.define do
  factory :image, class: ImageLoader::Image do
    sequence(:url) { |n| "#{Faker::Internet.url}_#{n}" }
  end
end
