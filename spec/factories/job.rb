FactoryGirl.define do
  factory :job, class: ImageLoader::Job do
    sequence(:url) { |n| "#{Faker::Internet.url}_#{n}" }

    trait :with_images do
      after(:create) do |job|
        create_list(:image, 3, job: job)
      end
    end
  end
end
