describe ImageLoader::Job do
  context 'count_images' do
    let!(:job) { create(:job, :with_images) }

    it 'returns all images current states as pending' do
      expect(job.count_images).to eq pending: job.images.size
    end

    it "updates count as images' statuses change" do
      job.images.first.failure!
      job.images.last.upload_started!
      expect(job.count_images).to eq loading: 1, failed: 1, pending: job.images.size - 2
    end
  end
end
