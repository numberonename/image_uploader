describe ImageLoader::Api::JobsApi do
  let!(:jobs) { create_list(:job, 3) }

  context 'get' do
    it 'returns all jobs' do
      get '/api/jobs'
      expect(last_body.size).to eq jobs.size
      last_body.each do |job|
        expect(job['id']).to be
      end
    end

    it 'returns a job by its id' do
      job = jobs.first
      get "/api/jobs/#{job.numeric_id}"
      expect(last_body['url']).to eq job.url
    end

    it 'returns a 404 if job with given id was not found' do
      get '/api/jobs/5f'
      expect(last_status).to eq 404
    end
  end

  context 'post' do
    it 'fails if no url was given' do
      post '/api/jobs', something: 1
      expect(last_status).to eq 400
      expect(last_body['message']).to match 'url'
    end

    it 'succeeds if all params are given' do
      post '/api/jobs', attributes_for(:job)
      expect(last_status).to eq 201
      expect(last_body['id']).to be
    end
  end

  context 'delete' do
    it 'returns a 404 if job with given id was not found' do
      delete '/api/jobs/5f'
      expect(last_status).to eq 404
    end

    it 'deletes a job by id' do
      job = jobs.first
      delete "/api/jobs/#{job.numeric_id}"
      expect(last_status).to eq 200
      expect(ImageLoader::Job.where(numeric_id: job.numeric_id).first).not_to be
    end
  end
end
