describe ImageLoader::Api::MainApi do
  it 'responds with 404 if page does not exist' do
    get '/api/unexisting'
    expect(last_status).to eq 404
  end

  it 'tests connection returning 200' do
    get '/api'
    expect(last_status).to eq 200
  end
end
