describe ImageLoader::Api::JobsApi do
  let!(:job) { create(:job, :with_images) }
  let!(:start_url) { "/api/jobs/#{job.numeric_id}/images" }

  context 'images' do
    it 'gets list of all jobs images' do
      get start_url
      expect(last_status).to eq 200
      expect(last_body.size).to eq job.images.size
      expect(last_body.size).to be > 0
      last_body.each do |img|
        expect(img['id']).to be
      end
    end

    it 'gets specific image by id' do
      img = job.images.first
      get "#{start_url}/#{img.numeric_id}"
      expect(last_body['source_url']).to eq img.url
    end

    it 'returns a 404 if img with given id was not found' do
      get "#{start_url}/12h"
      expect(last_status).to eq 404
    end
  end
end
