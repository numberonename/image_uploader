ENV['IMAGE_LOADER_ENV'] = 'test'
require './config/environments/all'

require 'goliath/test_helper'
require 'database_cleaner'
require 'faker'
require 'factory_girl'
require 'rack/test'

Dir.glob("#{Dir.pwd}/spec/factories/*").each {|f| require f }

RSpec.configure do |config|
  config.include Goliath::TestHelper
  config.include Rack::Test::Methods
  config.include FactoryGirl::Syntax::Methods

  config.before(:suite) do
    begin
      DatabaseCleaner.start
      FactoryGirl.lint
    ensure
      DatabaseCleaner.clean
    end
  end

  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
end

def app
  ImageLoader::Api::MainApi
end

def last_body
  JSON.parse(last_response.body)
end

def last_status
  last_response.status
end
