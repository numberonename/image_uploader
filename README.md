## Installation
### With Vagrant
0. `vagrant up`
0. The server will start at port 5000. Inside the box the image loader is wrapped into a service `image_loader`

### Without Vagrant
0. Install rvm, ruby 2.2.2, imagemagick, redis, and mongodb
0. `cd` into project directory
0. run `bundle`
0. run `foreman start`. By default the server starts at port 5000, if you need to change it, pass `PORT`
environment variable: `PORT=7000 foreman start`


## Accessing the API and the docs
The API can be accessed at '/api', the swagger documentation for the API can be viewed at '/docs'


## Scaling
The app can be easily scaled:

 - to store images in the cloud: the images are stored with help of Paperclip, that
  supports AWS out of the box. All one would need to do is add `aws-sdk` to the Gemfile and
  pass s3 options in `ImageLoader::Image` - `has_mongoid_attached_file`:

        has_mongoid_attached_file :attachment,
            # options
            storage: :s3,
            url: ':s3_alias_url',
            s3_host_alias: 'something.cloudfront.net',
            s3_credentials: path-to-s3-credentials.yml
            # options

 - to distribute jobs among multiple Redis servers: we would need to use `Redis::Distributed`
  in config/sidekiq.rb to configure Sidekiq to pass jobs in a round-robin manner across Redis instances
