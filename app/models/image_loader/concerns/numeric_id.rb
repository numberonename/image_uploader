module ImageLoader
  module NumericId
    extend ActiveSupport::Concern

    included do
      include Mongoid::Autoinc

      field :numeric_id, type: Integer
      increments :numeric_id
    end
  end
end
