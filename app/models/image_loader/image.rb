require_relative 'concerns/numeric_id'

module ImageLoader
  class Image
    include Mongoid::Document
    include Mongoid::Paperclip
    include Mongoid::Enum
    include NumericId
    include AASM

    ALLOWED_TYPES = %w(
      image/jpg image/jpeg image/png image/gif image/bmp image/tiff
    )

    enum :state, %i(pending loading done failed)

    field :state, type: Integer
    field :width, type: Integer
    field :height, type: Integer
    field :size, type: Integer
    field :url, type: String
    field :last_error, type: String

    has_mongoid_attached_file :attachment,
                              url: '/system/jobs/:job_id/images/:nid/:style/:nid.:ext_from_type',
                              path: ':il_root/public/system/jobs/:job_id/images/:nid/:style/:nid.:ext_from_type',
                              convert_options: { all: '-strip -auto-orient -colorspace sRGB' }

    validates_attachment :attachment, content_type:
                                      { content_type: ALLOWED_TYPES }

    after_create :process
    before_save :store_image_data

    belongs_to :job

    aasm column: :state, enum: true do
      state :pending, initial: true
      state :loading
      state :done
      state :failed

      event :upload_started do
        transitions from: :pending, to: :loading
      end

      event :success do
        transitions from: :loading, to: :done
      end

      event :failure do
        transitions to: :failed
      end
    end

    def as_json(options = nil)
      {
        id: numeric_id,
        source_url: url,
        state: state,
        our_url: attachment.present? ? attachment.url(:original) : nil,
        server_path: attachment.present? ? attachment.path : nil,
        job_id: job.numeric_id,
        width: width,
        height: height,
        size: size,
        content_type: attachment_content_type,
        last_error: last_error
      }.reject { |_, v| v.nil? }
    end

    def process
      ImageWorker.perform_async(id.to_s)
    end

    def store_image_data
      tempfile = attachment.queued_for_write[:original]
      return unless tempfile
      geometry = Paperclip::Geometry.from_file(tempfile)
      self.width = geometry.width.to_i
      self.height = geometry.height.to_i
      self.size = tempfile.size
    end
  end
end
