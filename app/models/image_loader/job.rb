require_relative 'concerns/numeric_id'

module ImageLoader
  class Job
    include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::Enum
    include NumericId
    include AASM

    enum :state, %i(pending images_loading parsed_page failed)

    default_scope -> { order(created_at: :desc) }

    field :url, type: String
    field :last_error, type: String

    has_many :images, dependent: :destroy

    after_create :process

    aasm column: :state, enum: true do
      state :pending, initial: true
      state :images_loading
      state :parsed_page
      state :failed

      event :ready_for_upload do
        transitions from: :pending, to: :images_loading
      end

      event :success do
        transitions from: :images_loading, to: :parsed_page
      end

      event :failure do
        transitions to: :failed
      end
    end

    def as_json(options = nil)
      {
        id: numeric_id,
        state: state,
        url: url,
        created_at: created_at,
        count_images: count_images,
        last_error: last_error
      }.reject { |_, v| v.nil? }
    end

    def process
      JobWorker.perform_async(id.to_s)
    end

    def count_images
      images.group_by(&:state).to_a.map { |k, v| [k, v.size] }.to_h
    end
  end
end
