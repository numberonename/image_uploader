module ImageLoader
  class ImageWorker
    include Sidekiq::Worker
    sidekiq_options retry: false

    def perform(id)
      Image.find(id).tap do |i|
        i.upload_started!
        i.attachment = i.url
        if i.valid?
          i.success
        else
          i.last_error = i.errors.full_messages.join('; ')
          i.failure
          i.attachment = nil
        end
        i.save
      end
    end
  end
end
