module ImageLoader
  class JobWorker
    include Sidekiq::Worker
    sidekiq_options retry: false

    def perform(id)
      pr = JobProcessor.new(id)
      pr.process_images
    end
  end
end
