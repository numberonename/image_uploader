module ImageLoader
  module Api
    class JobsApi < Grape::API
      resource :jobs do
        desc 'return all jobs with images'
        get do
          Job.all
        end

        desc 'create a job and start loading images from it'
        params do
          requires :url, type: String, desc: 'URL of job to parse images from'
        end
        post do
          Job.create!(permitted_params)
        end

        delete do
          # for debugging
          Job.all.each &:destroy
        end

        segment ':id' do
          before do
            @job = Job.find_by(numeric_id: params[:id])
          end

          desc 'return specific job with images by its id'
          get do
            @job.as_json.merge(images: @job.images)
          end

          desc 'destroy job and all its images'
          delete do
            @job.destroy!
            {}
          end

          resource :images do
            desc 'return data about all images of given job'
            get do
              @job.images
            end

            desc 'return data about specific image of job'
            get ':image_id' do
              @job.images.find_by(numeric_id: params[:image_id])
            end
          end
        end
      end
    end
  end
end
