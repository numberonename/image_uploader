module ImageLoader
  module Api
    class MainApi < Grape::API
      prefix 'api'
      default_format :json

      helpers ApiHelper

      server_logger = logger

      rescue_from Mongoid::Errors::Validations do |e|
        rack_response({ error: true,
                        message: e.record.errors.full_messages.join('; '),
                        status: 400
                      }.to_json, 400)
      end

      rescue_from Mongoid::Errors::DocumentNotFound do |e|
        rack_response({ error: true,
                        message: 'Not found',
                        status: 404
                      }.to_json, 404)
      end

      rescue_from :all do |e|
        status, message = (e.status rescue 500), (e.message rescue 'Server Error')

        if status == 500
          server_logger.error("== API error == #{e.class}: #{e.message} \n #{e.backtrace.join("\n")}") if status == 500
          message = 'Server Error'
        end

        rack_response({ error: true,
                        message: message,
                        status: status
                      }.to_json, status)
      end

      [JobsApi].each do |api_class|
        mount api_class
      end

      add_swagger_documentation	base_path: lambda {|req| "http://#{req.host}:#{req.port}" },
                                 hide_format: true

      desc 'test connection to API'
      get do; end

      route :any, '*path' do
        error!({ error: true, message: '', status: 404 }, 404)
      end
    end
  end
end
