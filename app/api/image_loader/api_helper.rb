module ImageLoader
  module ApiHelper
    ERRORS = { '400' => 'Bad request', '404' => 'Not found' }

    class GrapeError < StandardError
      attr_reader :status

      def initialize status
        @status = status
      end
    end

    def grape_error! message, status
      raise GrapeError.new(status), message
    end

    ERRORS.each do |code, mess|
      define_method("e#{code}") do |message=mess|
        grape_error!(message, code.to_i)
      end
    end

    def permitted_params
      @permitted_params ||= declared(params, include_missing: false).to_h
    end
  end
end
